# Gitコマンド実行時出力のページャ機能について
ページャ設定は、シェルに依存する。 
実行結果を閲覧しながら次のコマンドを思考することがあるため、lessコマンドで結果が出力されるとそれができない。  
## 出力内容をページャ機能を使用しないコマンド
```
git --no-pager コマンド
```
## lessコマンド
linuxコマンドで定義されているコマンド  
テキストファイルを1画面で表示する。
## 設定ファイル(config)にページャ設定をする
毎度上記コマンド実行が面倒な時は、設定ファイルにページャコマンドを設定する。

```
git config --global core.pager 'cat'
```
## コマンドごとに設定したい時
(例)logコマンドのみcat表示
```
git config --global pager.log 'cat'
```
# 参考サイト
[Git config document](https://git-scm.com/docs/git-config)